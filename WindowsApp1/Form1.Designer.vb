﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.origen = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.destino = New System.Windows.Forms.TextBox()
        Me.grupoOrigen = New System.Windows.Forms.GroupBox()
        Me.oriHexa = New System.Windows.Forms.RadioButton()
        Me.oriBinario = New System.Windows.Forms.RadioButton()
        Me.oriDecimal = New System.Windows.Forms.RadioButton()
        Me.grupoDestino = New System.Windows.Forms.GroupBox()
        Me.destHexa = New System.Windows.Forms.RadioButton()
        Me.destDecimal = New System.Windows.Forms.RadioButton()
        Me.destBinario = New System.Windows.Forms.RadioButton()
        Me.limpiar = New System.Windows.Forms.Button()
        Me.calcular = New System.Windows.Forms.Button()
        Me.grupoOrigen.SuspendLayout()
        Me.grupoDestino.SuspendLayout()
        Me.SuspendLayout()
        '
        'origen
        '
        Me.origen.Location = New System.Drawing.Point(94, 30)
        Me.origen.Name = "origen"
        Me.origen.Size = New System.Drawing.Size(100, 20)
        Me.origen.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(50, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Origen"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(269, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Destino"
        '
        'destino
        '
        Me.destino.Enabled = False
        Me.destino.Location = New System.Drawing.Point(318, 30)
        Me.destino.Name = "destino"
        Me.destino.Size = New System.Drawing.Size(100, 20)
        Me.destino.TabIndex = 2
        '
        'grupoOrigen
        '
        Me.grupoOrigen.Controls.Add(Me.oriHexa)
        Me.grupoOrigen.Controls.Add(Me.oriBinario)
        Me.grupoOrigen.Controls.Add(Me.oriDecimal)
        Me.grupoOrigen.Location = New System.Drawing.Point(65, 87)
        Me.grupoOrigen.Name = "grupoOrigen"
        Me.grupoOrigen.Size = New System.Drawing.Size(129, 120)
        Me.grupoOrigen.TabIndex = 4
        Me.grupoOrigen.TabStop = False
        Me.grupoOrigen.Text = "Sistema origen:"
        '
        'oriHexa
        '
        Me.oriHexa.AutoSize = True
        Me.oriHexa.Location = New System.Drawing.Point(6, 75)
        Me.oriHexa.Name = "oriHexa"
        Me.oriHexa.Size = New System.Drawing.Size(86, 17)
        Me.oriHexa.TabIndex = 2
        Me.oriHexa.TabStop = True
        Me.oriHexa.Text = "Hexadecimal"
        Me.oriHexa.UseVisualStyleBackColor = True
        '
        'oriBinario
        '
        Me.oriBinario.AutoSize = True
        Me.oriBinario.Location = New System.Drawing.Point(6, 52)
        Me.oriBinario.Name = "oriBinario"
        Me.oriBinario.Size = New System.Drawing.Size(57, 17)
        Me.oriBinario.TabIndex = 1
        Me.oriBinario.TabStop = True
        Me.oriBinario.Text = "Binario"
        Me.oriBinario.UseVisualStyleBackColor = True
        '
        'oriDecimal
        '
        Me.oriDecimal.AutoSize = True
        Me.oriDecimal.Location = New System.Drawing.Point(6, 29)
        Me.oriDecimal.Name = "oriDecimal"
        Me.oriDecimal.Size = New System.Drawing.Size(63, 17)
        Me.oriDecimal.TabIndex = 0
        Me.oriDecimal.TabStop = True
        Me.oriDecimal.Text = "Decimal"
        Me.oriDecimal.UseVisualStyleBackColor = True
        '
        'grupoDestino
        '
        Me.grupoDestino.Controls.Add(Me.destHexa)
        Me.grupoDestino.Controls.Add(Me.destDecimal)
        Me.grupoDestino.Controls.Add(Me.destBinario)
        Me.grupoDestino.Location = New System.Drawing.Point(289, 87)
        Me.grupoDestino.Name = "grupoDestino"
        Me.grupoDestino.Size = New System.Drawing.Size(129, 120)
        Me.grupoDestino.TabIndex = 5
        Me.grupoDestino.TabStop = False
        Me.grupoDestino.Text = "Sistema destino:"
        '
        'destHexa
        '
        Me.destHexa.AutoSize = True
        Me.destHexa.Location = New System.Drawing.Point(6, 75)
        Me.destHexa.Name = "destHexa"
        Me.destHexa.Size = New System.Drawing.Size(86, 17)
        Me.destHexa.TabIndex = 5
        Me.destHexa.TabStop = True
        Me.destHexa.Text = "Hexadecimal"
        Me.destHexa.UseVisualStyleBackColor = True
        '
        'destDecimal
        '
        Me.destDecimal.AutoSize = True
        Me.destDecimal.Location = New System.Drawing.Point(6, 29)
        Me.destDecimal.Name = "destDecimal"
        Me.destDecimal.Size = New System.Drawing.Size(63, 17)
        Me.destDecimal.TabIndex = 3
        Me.destDecimal.TabStop = True
        Me.destDecimal.Text = "Decimal"
        Me.destDecimal.UseVisualStyleBackColor = True
        '
        'destBinario
        '
        Me.destBinario.AutoSize = True
        Me.destBinario.Location = New System.Drawing.Point(6, 52)
        Me.destBinario.Name = "destBinario"
        Me.destBinario.Size = New System.Drawing.Size(57, 17)
        Me.destBinario.TabIndex = 4
        Me.destBinario.TabStop = True
        Me.destBinario.Text = "Binario"
        Me.destBinario.UseVisualStyleBackColor = True
        '
        'limpiar
        '
        Me.limpiar.Location = New System.Drawing.Point(82, 239)
        Me.limpiar.Name = "limpiar"
        Me.limpiar.Size = New System.Drawing.Size(75, 23)
        Me.limpiar.TabIndex = 6
        Me.limpiar.Text = "Limpiar"
        Me.limpiar.UseVisualStyleBackColor = True
        '
        'calcular
        '
        Me.calcular.Location = New System.Drawing.Point(318, 239)
        Me.calcular.Name = "calcular"
        Me.calcular.Size = New System.Drawing.Size(75, 23)
        Me.calcular.TabIndex = 7
        Me.calcular.Text = "Calcular"
        Me.calcular.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 306)
        Me.Controls.Add(Me.calcular)
        Me.Controls.Add(Me.limpiar)
        Me.Controls.Add(Me.grupoDestino)
        Me.Controls.Add(Me.grupoOrigen)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.destino)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.origen)
        Me.Name = "Form1"
        Me.Text = "Conversor"
        Me.grupoOrigen.ResumeLayout(False)
        Me.grupoOrigen.PerformLayout()
        Me.grupoDestino.ResumeLayout(False)
        Me.grupoDestino.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents origen As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents destino As TextBox
    Friend WithEvents grupoOrigen As GroupBox
    Friend WithEvents oriHexa As RadioButton
    Friend WithEvents oriBinario As RadioButton
    Friend WithEvents oriDecimal As RadioButton
    Friend WithEvents grupoDestino As GroupBox
    Friend WithEvents destHexa As RadioButton
    Friend WithEvents destDecimal As RadioButton
    Friend WithEvents destBinario As RadioButton
    Friend WithEvents limpiar As Button
    Friend WithEvents calcular As Button
End Class
