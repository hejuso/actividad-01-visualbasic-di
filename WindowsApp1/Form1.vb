﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles limpiar.Click

        origen.Text = ""
        oriBinario.Checked = False
        oriDecimal.Checked = False
        oriHexa.Checked = False

        destino.Text = ""
        destBinario.Checked = False
        destDecimal.Checked = False
        destHexa.Checked = False

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles calcular.Click

        If Not grupoDestino.Controls.OfType(Of RadioButton).Any(Function(cb) cb.Checked) Or Not grupoOrigen.Controls.OfType(Of RadioButton).Any(Function(cb) cb.Checked) Or origen.Text.Length < 1 Then
            MsgBox("No ha rellenado todas las opciones")
        Else
            Dim datoOrigen As String = origen.Text
            Dim datoDestino
            Dim datoConvertido As Integer

            If oriDecimal.Checked Then
                If IsNumeric(datoOrigen) Then
                    datoConvertido = System.Convert.ToInt32(datoOrigen, 10)
                Else
                    MsgBox("Introduzca un valor numérico")
                End If
            End If

            If oriBinario.Checked Then
                If IsNumeric(datoOrigen) Then
                    datoConvertido = System.Convert.ToInt32(datoOrigen, 2)
                Else
                    MsgBox("Introduzca un valor numérico")
                End If
            End If

            If oriHexa.Checked Then
                datoConvertido = System.Convert.ToInt32(datoOrigen, 16)
            End If

            If destDecimal.Checked Then
                If destDecimal.Checked And oriDecimal.Checked Then
                    MsgBox("No se puede convertir de Hexadecimal a Hexadecimal")
                Else
                    datoDestino = System.Convert.ToInt32(datoConvertido, 10)
                End If
            End If

            If destBinario.Checked Then
                If oriBinario.Checked And destBinario.Checked Then
                    MsgBox("No se puede convertir de binario a binario")
                Else
                    datoDestino = System.Convert.ToString(datoConvertido, 2)
                End If
            End If

            If destHexa.Checked Then
                If destHexa.Checked And oriHexa.Checked Then
                    MsgBox("No se puede convertir de Hexadecimal a Hexadecimal")
                Else
                    datoDestino = Hex(datoConvertido)
                End If
            End If

            destino.Text = datoDestino

        End If
    End Sub

End Class
